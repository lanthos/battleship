""" Client for connecting to Battleship server """

import socket
import sys

from battleship import Battleship, Player


class BattleshipClient(Battleship):
    """ Client for connecting to Battleship server """
    def __init__(self, *args, **kwargs):
        super(BattleshipClient, self).__init__(*args, **kwargs)

        player_name = self._get_input("What's your name? ")
        self.me = Player(player_name)

        self.opponent = Player('the computer')

    def start_game(self):
        self._connect()
        self._send_player_name()
        self._start_game_loop()

    def _start_game_loop(self):
        """ Client game mechanics """

        while not self.done:
            print("Current hits and misses: \n")
            self.print_board()
            print('')
            choice = self._get_input(
                'What tile would you like to attack?  E.G. "D3": ')

            if choice.lower() == 'done':
                self._s.sendall(choice + '|' + self.opponent_hit)
                break

            self._s.sendall(choice + '|' + self.opponent_hit)
            results, opponent_attack = self._s.recv(1024).split('|')

            if 'hit' in results:
                self.playing_board[choice[0:1]][int(choice[1:])-1] = "H"
            if 'miss' in results:
                self.playing_board[choice[0:1]][int(choice[1:])-1] = "M"
            if results == "done":
                self._display("\nYou destroyed all of my ships!")
                break

            self._display(results)
            self._display("\nIt's {0}'s turn!".format(self.opponent.name))
            opponent_turn = self.defend(opponent_attack, self.opponent)
            if 'hit' in opponent_turn:
                self.opponent_hit = '1'
            if 'miss' in opponent_turn:
                self.opponent_hit = '0'
            if opponent_turn == 'done':
                self._display("\n{0} destroyed all of your ships.".format(
                    self.opponent.name.capitalize()))
                self._s.sendall('done')
                break
            self._display(opponent_turn)

    def _connect(self):
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.connect((self.server_host, self.server_port))

    def _disconnect(self):
        self._s.close()

    def _send_player_name(self):
        self._s.sendall('__PLAYER__|' + self.me.name)


battleship_client = BattleshipClient()
battleship_client.start_game()