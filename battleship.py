""" Base Battleship class for client and server """

import config as _c


class Battleship(object):
    """ Base Battleship class for client and server """
    def __init__(self, server_host=_c.SERVER_HOST, server_port=_c.SERVER_PORT):
        self.server_host = server_host
        self.server_port = server_port

        # TODO: allow fleet to be set randomly or by init input?
        self.fleet = {
            "carrier": ['E1', 'F1', 'G1', 'H1', 'I1'],
            "battleship": ['C6', 'C7', 'C8', 'C9'],
            "submarine": ['A3', 'B3', 'C3'],
            "cruiser": ['J6', 'J7', 'J8'],
            "destroyer": ['E5', 'E6']
        }
        self.wrecks = []
        self.cols = _c.BOARD_COLUMNS  # TODO: allow these to be set on init
        self.rows = _c.BOARD_ROWS     #       with config as defaults?
        self.playing_board = self.create_board()
        # Renamed to opponent, later maybe 2 clients playing through server
        self.opponent_hit = '0'
        self.done = False

    def create_board(self):
        """ Initialize playing board """

        board = {}
        for col in self.cols:
            board[col] = []
            for n in range(1, 11):
                board[col].append(n)
        return board

    def print_board(self):
        for k, v in self.playing_board.items():
            print("{}: {}".format(k, v))

    def defend(self, coord, attacker):
        """ Respond to attack """

        self._display('Incoming attack at {0}!'.format(coord.upper()))

        hits = 0
        for ship in self.fleet:
            if coord.upper() in self.fleet[ship]:
                hits += 1
                self.fleet[ship].remove(coord.upper())
                if not self.fleet[ship]:
                    self.wrecks.append(ship)
                break

        if hits == 0:
            message = '\n{0} missed!'.format(attacker.name.capitalize())
            return message

        if len(self.wrecks) == len(self.fleet):
            message = 'done'
            return message

        message = ("\nThat's a hit!  "
                   "\n{name}'s destroyed ships: {wrecks}".format(
                       name=self.me.name.capitalize(), wrecks=self.wrecks))
        return message

    def _get_input(self, message):
        """ Get input from player """
        # Because maybe later it won't just be a raw_input
        return raw_input(message)

    def _display(self, message):
        """ Display message to player """
        # Because maybe later it won't just be a print statement
        print message


class Player(object):
    """ Player """
    def __init__(self, name):
        self.name = name