""" Server for playing the game battleship """

import logging
import random
import socket

from battleship import Battleship, Player

LOG_FORMAT = '%(asctime)-15s (%(name)s) %(levelname)s: %(message)s'
logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
logger = logging.getLogger('BattleshipServer')


class BattleshipServer(Battleship):
    """ Server for playing the game battleship """

    def __init__(self, *args, **kwargs):
        super(BattleshipServer, self).__init__(*args, **kwargs)

        self.opponent_guesses = []
        self.opponent_hits = []
        self.opponent_attack = '&1'
        self.previous_hit = '&1'
        self.miss_count = 0
        self.hit_count = 0
        self.player_guess = None
        self.me = Player('the computer')
        self.player = None

    def create_board(self):
        """ Initialize playing board """

        board = []
        for col in self.cols:
            for row in self.rows:
                board.append(col + row)
        return board

    def start_game(self):
        self._start_server()
        self._start_game_loop()

    def attack(self, opponent_hit, hit_count, miss_count):
        """ AI to attack player """

        while True:

            col = self.previous_hit[:1]
            row = self.previous_hit[1:]

            if not opponent_hit and hit_count > 0:
                hit_before_last = self.opponent_hits[-2:-1]
                hbl_col = str(hit_before_last)[2:3]
                hbl_row = str(hit_before_last)[3:4]
                if col == hbl_col:
                    if int(row) - int(hbl_row) == 1:
                        guesses = [
                            (col, str(int(hbl_row) - 1)),
                        ]
                    else:
                        guesses = [
                            (col, str(int(hbl_row) + 1)),
                        ]
                    for c, r in guesses:
                        possible_guess = c in self.cols and r in self.rows
                        guess = '{0}{1}'.format(c, r)
                        if possible_guess and guess not in self.opponent_guesses:
                            self.opponent_guesses.append(guess)
                            return guess
                        else:
                            guess = random.choice(self.playing_board)
                            if guess not in self.opponent_guesses:
                                self.opponent_guesses.append(guess)
                                return guess
                if row == hbl_row:
                    if ord(row) - int(hbl_row) == 1:
                        guesses = [
                            (chr(ord(hbl_col) - 1), row)
                        ]
                    else:
                        guesses = [
                            (chr(ord(hbl_col) + 1), row)
                        ]
                    for c, r in guesses:
                        possible_guess = c in self.cols and r in self.rows
                        guess = '{0}{1}'.format(c, r)
                        if possible_guess and guess not in self.opponent_guesses:
                            self.opponent_guesses.append(guess)
                            return guess
                        else:
                            guess = random.choice(self.playing_board)
                            if guess not in self.opponent_guesses:
                                self.opponent_guesses.append(guess)
                                return guess

            if not opponent_hit and miss_count > 0 < 5:
                guesses = [
                    (col, str(int(row) - 1)),
                    (col, str(int(row) + 1)),
                    (chr(ord(col) - 1), row),
                    (chr(ord(col) + 1), row)
                ]
                for c, r in guesses:
                    possible_guess = c in self.cols and r in self.rows
                    guess = '{0}{1}'.format(c, r)
                    if possible_guess and guess not in self.opponent_guesses:
                        self.opponent_guesses.append(guess)
                        return guess

            if opponent_hit and hit_count > 1:
                guess_before_last = self.opponent_guesses[-1:]
                gbl_col = str(guess_before_last)[2:3]
                gbl_row = str(guess_before_last)[3:4]
                if col == gbl_col:
                    if int(row) - int(gbl_row) == 1:
                        guesses = [
                            (col, str(int(row) - 1)),
                        ]
                    else:
                        guesses = [
                            (col, str(int(row) + 1)),
                        ]
                    for c, r in guesses:
                        possible_guess = c in self.cols and r in self.rows
                        guess = '{0}{1}'.format(c, r)
                        if possible_guess and guess not in self.opponent_guesses:
                            self.opponent_guesses.append(guess)
                            return guess
                        else:
                            guess = random.choice(self.playing_board)
                            if guess not in self.opponent_guesses:
                                self.opponent_guesses.append(guess)
                                return guess
                if row == gbl_row:
                    if ord(row) - int(gbl_row) == 1:
                        guesses = [
                            (chr(ord(col) - 1), row)
                        ]
                    else:
                        guesses = [
                            (chr(ord(col) + 1), row)
                        ]
                    for c, r in guesses:
                        possible_guess = c in self.cols and r in self.rows
                        guess = '{0}{1}'.format(c, r)
                        if possible_guess and guess not in self.opponent_guesses:
                            self.opponent_guesses.append(guess)
                            return guess
                        else:
                            guess = random.choice(self.playing_board)
                            if guess not in self.opponent_guesses:
                                self.opponent_guesses.append(guess)
                                return guess

            if opponent_hit:
                guesses = [
                    (col, str(int(row) - 1)),
                    (col, str(int(row) + 1)),
                    (chr(ord(col) - 1), row),
                    (chr(ord(col) + 1), row)
                ]
                for c, r in guesses:
                    possible_guess = c in self.cols and r in self.rows
                    guess = '{0}{1}'.format(c, r)
                    if possible_guess and guess not in self.opponent_guesses:
                        self.opponent_guesses.append(guess)
                        return guess

            if not opponent_hit:  # and miss_count == 0:
                # TODO: Is there a more efficient way to do this then picking
                #       randomly in a loop until finding a possible guess?
                guess = random.choice(self.playing_board)
                if guess not in self.opponent_guesses:
                    self.opponent_guesses.append(guess)
                    return guess

    def _start_game_loop(self):
        """ Server game mechanics """

        while not self.done:
            cmd, arg = self._conn.recv(1024).split('|')

            if cmd == '__PLAYER__':
                self.player = Player(arg)
                continue

            self.player_guess = cmd
            self.opponent_hit = arg

            self.opponent_hit = bool(int(self.opponent_hit))

            if self.player_guess.upper() == 'DONE':
                break

            results = self.defend(self.player_guess, self.player)

            if results == 'done':
                self._conn.sendall('done')
                break
            elif 'hit' in results:
                logger.info('HIT!')
            elif 'miss' in results:
                logger.info('Miss.')

            if not self.opponent_hit:
                self.miss_count += 1
                if self.miss_count > 1:
                    self.hit_count = 0
            else:
                self.miss_count = 0
                self.hit_count += 1
                self.opponent_hits.append(self.opponent_attack)
                self.previous_hit = self.opponent_attack

            self.opponent_attack = self.attack(self.opponent_hit, self.hit_count, self.miss_count)
            logger.info('Attacking at {0}'.format(self.opponent_attack))
            self._conn.sendall(results + '|' + self.opponent_attack)

        self._stop_server()

    def _start_server(self):
        logger.info('Starting server...')
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._s.bind((self.server_host, self.server_port))
        self._s.listen(5)
        self._conn, self._addr = self._s.accept()
        logger.info('Connected by {0}'.format(self._addr))

    def _stop_server(self):
        self._conn.close()


battleship_server = BattleshipServer()
battleship_server.start_game()