# README #

### What is this repository for? ###

* Battleship learning project
* Version: 0.001

### How do I get set up? ###

* Download the .py files into the same directory.  Edit config.py and follow directions for single host or multi-host.  Start the server first and then the client.

### Who do I talk to? ###

* Jeremy Kenyon - lanthos@gmail.com
