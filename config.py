""" Config for Battleship game """

import socket


SERVER_HOST = socket.gethostbyname(socket.gethostname())  # for playing on a single machine
#  SERVER_HOST = '10.0.1.6'  #  for playing with another machine.  Use this line for client machine.
SERVER_PORT = 50007

BOARD_COLUMNS = 'A B C D E F G H I J'.split()
BOARD_ROWS = '1 2 3 4 5 6 7 8 9 10'.split()
